﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;
    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidBody;
    int floorMask;
    float camRayLength = 100f;


    private void Awake()
    {
        // mendapatkan nilai mask dari layer yang bernama Floor
        floorMask = LayerMask.GetMask("Floor");

        // mendapatkan komponen Animator
        anim = GetComponent<Animator>();

        // mendapatkan komponen RigidBody
        playerRigidBody = GetComponent<Rigidbody>();
    }


    private void FixedUpdate()
    {
        // mendapatkan nilai input horizontal (-1, 0, 1)
        float h = Input.GetAxisRaw("Horizontal");

        // mendapatkan nilai input vertical (-1, 0, 1)
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }


    public void Move(float h, float v)
    {
        // nilai x dan y Vector3 movement
        movement.Set(h, 0, v);

        // di-normalisasi menjadikan panjang atau magnitude vectornya menjadi 1 (untuk arah)
        movement = movement.normalized * speed * Time.deltaTime;

        // Move to new position
        playerRigidBody.MovePosition(transform.position + movement);
    }


    private void Turning()
    {
        // buat ray dari posisi mouse di layar
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // buat raycast untuk floorHit
        RaycastHit floorHit;

        // lakukan raycast
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            // mendapatkan vector dari posisi player ke posisi floorHit
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            // mendapatkan rotation baru yang menghadap ke titik floorHit
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // rotasi player
            playerRigidBody.MoveRotation(newRotation);
        }
    }


    public void Animating(float h, float v)
    {
        bool walking = h != 0f || v != 0f;
        anim.SetBool("IsWalking", walking);
    }
}
