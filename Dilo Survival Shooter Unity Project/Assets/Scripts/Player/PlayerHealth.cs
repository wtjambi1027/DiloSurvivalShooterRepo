﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int startingHealth = 100;
    public int currentHealth;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColor = new Color(1f, 0f, 0f, 0.1f);

    Animator anim;
    AudioSource playerAudio;
    PlayerMovement playerMovement;
    PlayerShooting playerShooting;
    InputHandler inputHandler;
    bool isDead;
    bool damaged;


    private void Awake()
    {
        // mendapatkan reference component
        anim = GetComponent<Animator>();
        playerAudio = GetComponent<AudioSource>();
        playerMovement = GetComponent<PlayerMovement>();
        playerShooting = GetComponentInChildren<PlayerShooting>();
        inputHandler = GetComponent<InputHandler>();

        currentHealth = startingHealth;
    }


    private void Update()
    {
        // jika kena damage
        if (damaged)
        {
            // merubah warna damageImage menjadi flashColor
            damageImage.color = flashColor;
        }
        else
        {
            // fade out damageImage
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        // reset damaged
        damaged = false;
    }


    // fungsi untuk mengenakan damage
    public void TakeDamage(int amount)
    {
        damaged = true;

        // mengurangi health
        currentHealth -= amount;

        // muengupdate nilai dan tampilan healthSlider
        healthSlider.value = currentHealth;

        // mainkan suara ketika kena damage
        playerAudio.Play();

        // jika belum mati dan darah dibawah 0
        if (currentHealth <= 0 && !isDead)
        {
            // panggil function Death()
            Death();
        }
    }


    void Death()
    {
        isDead = true;

        playerShooting.DisableEffects();

        // mentrigger animasi die
        anim.SetTrigger("Die");

        // memainkan audio ketika mati
        playerAudio.clip = deathClip;
        playerAudio.Play();

        // mematikan beberapa script pada player
        playerMovement.enabled = false;
        playerShooting.enabled = false;
        inputHandler.enabled = false;
    }


    public void RestartLevel()
    {
        // meload ulang scene dengan index 0 pada build setting 
        SceneManager.LoadScene(0);
    }
}
