﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;


    private void Awake()
    {
        // cari game object dengan tag player
        player = GameObject.FindGameObjectWithTag("Player").transform;

        // mendapatkan reference component"
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }


    private void Update()
    {
        // jika enemy dan player masih hidup
        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            // set tujuan menjadi posisi player
            nav.SetDestination(player.position);
        }
        else // jika tidak
        {
            // matikan navmesh (berhenti mencari dan bergerak)
            nav.enabled = false;
        }
    }
}
