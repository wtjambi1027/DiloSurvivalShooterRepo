﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public PlayerHealth playerHealth;
    //public GameObject enemy;
    public float spawnTime = 1f;
    //public Transform[] spawnPoints;

    [SerializeField]
    MonoBehaviour factory;
    IFactory Factory { get { return factory as IFactory; } }


    private void Start()
    {
        // memanggil function Spawn setiap spawnTime
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


    void Spawn()
    {
        // tidak melakukan spawn jika player tidak hidup
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }

        // mendapatkan nilai random
        int spawnEnemy = Random.Range(0, 3);
        //int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // menduplikasi enemy
        Factory.FactoryMethod(spawnEnemy);
        //Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}
